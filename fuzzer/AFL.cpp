#include <iostream>
#include <rili/JSON.hpp>
#include <string>

int main(int /*argc*/, char** /*argv*/) {
    rili::JSON j;
    std::string data(std::istreambuf_iterator<char>(std::cin), {});
    try {
        j.parse(data);
    } catch (rili::JSON::SyntaxError e) {
    }
    j.stringify();
    return 0;
}
