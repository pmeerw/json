#pragma once
/** @file */

#include <exception>
#include <list>
#include <rili/Variant.hpp>
#include <string>
#include <utility>
namespace rili {
/**
 * @brief The Value class is abstraction over JSON tree node.
 *
 * @note Number value is internally stored and parsed exactly the same like in original JSON string. Parser only
 * validate it against JSON requirements, as there is not precisely specified range, precision and other parameters of
 * Number value in JSON standards. Because of that we belive it's better to not force any C++ POD(double, int etc.) or
 * data structure(GMP BigNumbers) here - you will probably better know how to convert these numbers or meybe you even
 * don't need this conversion at all.
 *
 * @warning You should ensure that data that you are assigning as Number is in valid JSON number format.
 *
 * @note String JSON value and Object JSON value keys are parsed as utf-8 encoded string
 * @warning You should ensure that data that you are assigning as String or Object key is valid utf-8 encoded.
 */
class JSON final {
 public:
    /**
     * @brief The SyntaxError class is used to communicate about invalid JSON synthax
     */
    class SyntaxError final : public std::exception {
     public:
        /**
         * @brief SyntaxError used to construct exception with known position of error in original JSON string
         * @param position aproximate place where error was found
         */
        explicit SyntaxError(size_t position) noexcept;

        /**
         * @brief what is used to retrieve exception message
         * @return exception message
         */
        const char* what() const noexcept override;

        /**
         * @brief position used to retrieve error position
         * @return  postion
         */
        size_t position() const noexcept;
        virtual ~SyntaxError() = default;

        /* @cond INTERNAL */
        SyntaxError(SyntaxError const&) = default;
        SyntaxError& operator=(SyntaxError const&) = default;
        /* @endcond INTERNAL */

     private:
        SyntaxError() = delete;
        size_t m_position;
        std::string m_what;
    };

    /**
     * @brief ArrayType is type used to store json Arrays
     */
    typedef std::list<JSON> ArrayType;

    /**
     * @brief ObjectType is type used to store json Objects
     */
    typedef std::list<std::pair<std::string, JSON>> ObjectType;

    /**
     * @brief The Type enum used to deduce specialized type of JSON node
     */
    enum class Type { Null, Boolean, String, Number, Array, Object };

    /**
     * @brief type used to retrieve used specialized type of JSON node
     * @return node type
     */
    inline Type type() const noexcept { return m_type; }

    /**
     * @brief boolean retrieve internaly stored value of boolean type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline bool& boolean() {
        if (m_type != Type::Boolean) {
            throw std::bad_cast();
        }
        return m_storage.get<bool>();
    }

    /**
     * @brief string retrieve internaly stored value of string type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline std::string& string() {
        if (m_type != Type::String) {
            throw std::bad_cast();
        }
        return m_storage.get<std::string>();
    }

    /**
     * @brief number retrieve internaly stored value of number type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline std::string& number() {
        if (m_type != Type::Number) {
            throw std::bad_cast();
        }
        return m_storage.get<std::string>();
    }

    /**
     * @brief array retrieve internaly stored value of array type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline ArrayType& array() {
        if (m_type != Type::Array) {
            throw std::bad_cast();
        }
        return m_storage.get<ArrayType>();
    }

    /**
     * @brief object retrieve internaly stored value of object type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline ObjectType& object() {
        if (m_type != Type::Object) {
            throw std::bad_cast();
        }
        return m_storage.get<ObjectType>();
    }

    /**
     * @brief boolean retrieve internaly stored value of boolean type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline bool const& boolean() const {
        if (m_type != Type::Boolean) {
            throw std::bad_cast();
        }
        return m_storage.get<bool>();
    }

    /**
     * @brief string retrieve internaly stored value of string type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline std::string const& string() const {
        if (m_type != Type::String) {
            throw std::bad_cast();
        }
        return m_storage.get<std::string>();
    }

    /**
     * @brief number retrieve internaly stored value of number type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline std::string const& number() const {
        if (m_type != Type::Number) {
            throw std::bad_cast();
        }
        return m_storage.get<std::string>();
    }

    /**
     * @brief array retrieve internaly stored value of array type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline ArrayType const& array() const {
        if (m_type != Type::Array) {
            throw std::bad_cast();
        }
        return m_storage.get<ArrayType>();
    }

    /**
     * @brief object retrieve internaly stored value of object type
     *
     * @warning throw std::bad_cast if type not match to really used
     *
     * @return value
     */
    inline ObjectType const& object() const {
        if (m_type != Type::Object) {
            throw std::bad_cast();
        }
        return m_storage.get<ObjectType>();
    }

    /**
     * @brief null assign null value to current JSON object
     */
    inline void null() { m_type = Type::Null; }

    /**
     * @brief boolean assign bool value to current JSON object
     * @param b value to assign
     */
    inline void boolean(bool b) {
        m_type = Type::Boolean;
        m_storage.set<bool>(b);
    }

    /**
     * @brief string assign string value to current JSON object
     * @param s value to assign
     */
    inline void string(std::string const& s) {
        m_type = Type::String;
        m_storage.set<std::string>(s);
    }

    /**
     * @brief number assign number value to current JSON object
     * @param n value to assign
     */
    inline void number(std::string const& n) {
        m_type = Type::Number;
        m_storage.set<std::string>(n);
    }

    /**
     * @brief array assign array value to current JSON object
     * @param a value to assign
     */
    inline void array(ArrayType const& a) {
        m_type = Type::Array;
        m_storage.set<ArrayType>(a);
    }

    /**
     * @brief object assign object value to current JSON object
     * @param o value to assign
     */
    inline void object(ObjectType const& o) {
        m_type = Type::Object;
        m_storage.set<ObjectType>(o);  // NOLINT
    }

    /**
     * @brief Value default constructor initializes Value as
     */
    inline JSON() noexcept : m_type(Type::Null), m_storage() {}

    /**
     * @brief JSON constructs root node from json string
     * @param json input JSON formated utf-8 encoded string
     * @param maxDepth specify how deep nasted Objects/Arrays will be accepted
     */
    inline JSON(std::string const& json, size_t maxDepth = 100) : JSON() { parse(json, maxDepth); }

    /**
     * @brief parse is function used to covert JSON string to object tree representation
     * @param json input JSON formated utf-8 encoded string
     * @param maxDepth specify how deep nasted Objects/Arrays will be accepted
     */
    void parse(const std::string& json, size_t maxDepth = 100);

    /**
     * @brief stringify is function used to convert object tree structure into JSON string
     * @return utf-8 encoded JSON string
     */
    std::string stringify() const noexcept;

 private:
    Type m_type;
    Variant<bool, std::string, ArrayType, ObjectType> m_storage;
};
}  // namespace rili
