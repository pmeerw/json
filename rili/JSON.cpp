#include <algorithm>
#include <list>
#include <memory>
#include <rili/JSON.hpp>
#include <rili/MakeUnique.hpp>
#include <stack>
#include <string>
#include <utility>

/*
 * Materials:
 *   http://www.ecma-international.org/publications/files/ECMA-ST/ECMA-404.pdf
 *   http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf
 *   https://tools.ietf.org/rfc/rfc8259.txt
*/

namespace {
inline static void convertCodepoint(std::string& result, size_t codepoint, size_t position) {
    if (codepoint < 0x80) {
        result += std::string::value_type(codepoint);
    } else if (codepoint <= 0x7ff) {
        result += std::string::value_type(0xC0 | ((codepoint >> 6) & 0x1F));
        result += std::string::value_type(0x80 | (codepoint & 0x3F));
    } else if (codepoint <= 0xffff && !(codepoint >= 0xD800 && codepoint <= 0xDFFF)) {
        result += std::string::value_type(0xE0 | ((codepoint >> 12) & 0x0F));
        result += std::string::value_type(0x80 | ((codepoint >> 6) & 0x3F));
        result += std::string::value_type(0x80 | (codepoint & 0x3F));
    } else if (codepoint <= 0x10ffff) {
        result += std::string::value_type(0xF0 | ((codepoint >> 18) & 0x07));
        result += std::string::value_type(0x80 | ((codepoint >> 12) & 0x3F));
        result += std::string::value_type(0x80 | ((codepoint >> 6) & 0x3F));
        result += std::string::value_type(0x80 | (codepoint & 0x3F));
    } else {
        throw rili::JSON::SyntaxError(position);
    }
}

static constexpr char uMapping[0x20][7] = {
    "\\u0000", "\\u0001", "\\u0002", "\\u0003", "\\u0004", "\\u0005", "\\u0006", "\\u0007",
    "\\b",     "\\t",     "\\n",     "\\u000b", "\\f",     "\\r",     "\\u000e", "\\u000f",
    "\\u0010", "\\u0011", "\\u0012", "\\u0013", "\\u0014", "\\u0015", "\\u0016", "\\u0017",
    "\\u0018", "\\u0019", "\\u001a", "\\u001b", "\\u001c", "\\u001d", "\\u001e", "\\u001f",
};

inline static std::string& utf8ToJson(std::string& os, const std::string& s) noexcept {
    for (auto const& c : s) {
        if (c >= 0 && c <= 0x1f) {
            os += uMapping[static_cast<uint8_t>(c)];
        } else if (c == '"') {
            os += "\\\"";
        } else if (c == '\\') {
            os += "\\\\";
        } else {
            os += c;
        }
    }
    return os;
}
inline static size_t hexToDight(char x, size_t position) {
    if (x >= '0' && x <= '9') {
        return size_t(x - '0');
    } else if (x >= 'a' && x <= 'f') {
        return size_t(x - 'a' + 10);
    } else if (x >= 'A' && x <= 'F') {
        return size_t(x - 'A' + 10);
    } else {
        throw rili::JSON::SyntaxError(position);
    }
}
inline static size_t getCodePoint(const char* start, const char* max, size_t position) {
    if (start + 3 >= max) {
        throw rili::JSON::SyntaxError(position);
    }
    size_t c1 = hexToDight(start[0], position) << 12;
    size_t c2 = hexToDight(start[1], position) << 8;
    size_t c3 = hexToDight(start[2], position) << 4;
    size_t c4 = hexToDight(start[3], position);
    return c1 | c2 | c3 | c4;
}

inline static size_t validateUtf8(char const unsigned* s, size_t size) {
    /*
        Code Points        First Byte Second Byte Third Byte Fourth Byte
        U+0000..U+007F     00..7F
        U+0080..U+07FF     C2..DF     80..BF
        U+0800..U+0FFF     E0         A0..BF      80..BF
        U+1000..U+CFFF     E1..EC     80..BF      80..BF
        U+D000..U+D7FF     ED         80..9F      80..BF
        U+E000..U+FFFF     EE..EF     80..BF      80..BF
        U+10000..U+3FFFF   F0         90..BF      80..BF     80..BF
        U+40000..U+FFFFF   F1..F3     80..BF      80..BF     80..BF
        U+100000..U+10FFFF F4         80..8F      80..BF     80..BF
    */

    enum class State { Begin, C2DF, E0, ES, E1ECvEEEF, ED, F0, FS, FT, F1F3, F4 };
    State state = State::Begin;
    for (size_t i = 0; i < size; i++) {
        switch (state) {
            case State::Begin: {
                if (s[i] <= 0x7f) {
                } else if (s[i] >= 0xc2 && s[i] <= 0xdf) {
                    state = State::C2DF;
                } else if (s[i] == 0xE0) {
                    state = State::E0;
                } else if ((s[i] >= 0xe1 && s[i] <= 0xec) || (s[i] >= 0xee && s[i] <= 0xef)) {
                    state = State::E1ECvEEEF;
                } else if (s[i] == 0xed) {
                    state = State::ED;
                } else if (s[i] == 0xF0) {
                    state = State::F0;
                } else if (s[i] >= 0xf1 && s[i] < 0xf3) {
                    state = State::F1F3;
                } else if (s[i] == 0xf4) {
                    state = State::F4;
                } else {
                    return i;
                }
                break;
            }
            case State::C2DF: {
                if (s[i] >= 0x80 && s[i] <= 0xbf) {
                    state = State::Begin;
                } else {
                    return i;
                }
                break;
            }
            case State::E0: {
                if (s[i] >= 0xa0 && s[i] <= 0xbf) {
                    state = State::ES;
                } else {
                    return i;
                }
                break;
            }
            case State::E1ECvEEEF: {
                if (s[i] >= 0x80 && s[i] <= 0xbf) {
                    state = State::ES;
                } else {
                    return i;
                }
                break;
            }
            case State::ED: {
                if (s[i] >= 0x80 && s[i] <= 0x9f) {
                    state = State::ES;
                } else {
                    return i;
                }
                break;
            }
            case State::ES: {
                if (s[i] >= 0x80 && s[i] <= 0xbf) {
                    state = State::Begin;
                } else {
                    return i;
                }
                break;
            }
            case State::F0: {
                if (s[i] >= 0x90 && s[i] <= 0xbf) {
                    state = State::FS;
                } else {
                    return i;
                }
                break;
            }
            case State::F1F3: {
                if (s[i] >= 0x80 && s[i] <= 0xbf) {
                    state = State::FS;
                } else {
                    return i;
                }
                break;
            }
            case State::F4: {
                if (s[i] >= 0x80 && s[i] <= 0x8f) {
                    state = State::FS;
                } else {
                    return i;
                }
                break;
            }
            case State::FS: {
                if (s[i] >= 0x80 && s[i] <= 0xbf) {
                    state = State::FT;
                } else {
                    return i;
                }
                break;
            }
            case State::FT: {
                if (s[i] >= 0x80 && s[i] <= 0xbf) {
                    state = State::Begin;
                } else {
                    return i;
                }
                break;
            }
        }
    }
    return size;
}

inline static std::string jsonToUtf8(const char* cs, const size_t& len, size_t position) {
    std::string result;
    char const* start = cs;
    char const* max = cs + len;
    for (; cs < max; ++cs) {
        size_t errorPosition = position + static_cast<size_t>(cs - start);
        if (*cs == '\\') {
            ++cs;
            switch (*cs) {
                case 't': {
                    result += '\t';
                    break;
                }
                case 'b': {
                    result += '\b';
                    break;
                }
                case 'f': {
                    result += '\f';
                    break;
                }
                case 'n': {
                    result += '\n';
                    break;
                }
                case 'r': {
                    result += '\r';
                    break;
                }
                case '/': {
                    result += '/';
                    break;
                }
                case '\"': {
                    result += '\"';
                    break;
                }
                case '\\': {
                    result += '\\';
                    break;
                }
                case 'u': {
                    auto codepoint = getCodePoint(cs + 1, max, errorPosition);
                    if ((codepoint <= 0xD7FF) || (codepoint >= 0xE000 && codepoint <= 0xFFFF)) {
                        convertCodepoint(result, codepoint, errorPosition);
                        cs += 4;
                    } else if (codepoint >= 0xD800 && codepoint <= 0xDBFF && cs + 6 < max && *(cs + 5) == '\\' &&
                               *(cs + 6) == 'u') {
                        auto codepoint2 = getCodePoint(cs + 7, max, errorPosition);
                        if (codepoint2 >= 0xDC00 && codepoint2 <= 0xDFFF) {
                            convertCodepoint(result, (codepoint << 10) + codepoint2 - 0x35FDC00, errorPosition);
                            cs += 10;
                        } else {
                            throw rili::JSON::SyntaxError(errorPosition);
                        }
                    } else {
                        throw rili::JSON::SyntaxError(errorPosition);
                    }

                    break;
                }
                default: { throw rili::JSON::SyntaxError(errorPosition); }
            }
        } else if (*cs >= 0x0 && *cs <= 0x1F) {
            throw rili::JSON::SyntaxError(errorPosition);
        } else {
            result += *cs;
        }
    }
    return result;
}

class Handler final {
 public:
    typedef std::stack<Handler, std::list<Handler>> Stack;
    enum class Type { Root, Array, Object, None };
    enum class ExpectedTokenType { initial, value, separatorOrEnd, keyOrEnd, key, keySeparator };

    inline void handleNull(size_t position);
    inline void handleBool(bool b, size_t position);
    inline void handleNumber(std::string&& n, size_t position);
    inline void handleString(std::string&& s, size_t position);
    inline void handleObjectStart(size_t position, size_t maxDepth);
    inline void handleObjectEnd(size_t position);
    inline void handleArrayStart(size_t position, size_t maxDepth);
    inline void handleArrayEnd(size_t position);
    inline void handleElementSeparator(size_t position);
    inline void handleObjectSeparator(size_t position);

    inline Handler(rili::JSON& v, Handler::Stack& s, Type type) noexcept
        : m_type(type),
          m_value(v),
          m_stack(s),
          m_expectedToken(m_type == Type::Array ? ExpectedTokenType::initial : ExpectedTokenType::keyOrEnd),
          m_currentKey() {}
    ~Handler() = default;
    Handler() = delete;
    Handler(Handler const&) = delete;
    Handler& operator=(Handler const&) = delete;

 protected:
    Type m_type;
    rili::JSON& m_value;
    Stack& m_stack;
    ExpectedTokenType m_expectedToken;
    std::string m_currentKey;
};

void Handler::handleNull(size_t position) {
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::value || m_expectedToken == ExpectedTokenType::initial) {
                m_value.array().push_back(rili::JSON());
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::value) {
                m_value.object().emplace_back(std::pair<std::string, rili::JSON>(m_currentKey, rili::JSON()));
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Root: {
            m_value.null();
            m_type = Type::None;
            return;
        }
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleBool(bool b, size_t position) {
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::value || m_expectedToken == ExpectedTokenType::initial) {
                m_value.array().push_back(rili::JSON());
                m_value.array().back().boolean(b);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::value) {
                m_value.object().emplace_back(std::pair<std::string, rili::JSON>(m_currentKey, rili::JSON()));
                m_value.object().back().second.boolean(b);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Root: {
            m_value.boolean(b);
            m_type = Type::None;
            return;
        }
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleNumber(std::string&& n, size_t position) {
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::value || m_expectedToken == ExpectedTokenType::initial) {
                m_value.array().push_back(rili::JSON());
                m_value.array().back().number(n);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::value) {
                m_value.object().emplace_back(std::pair<std::string, rili::JSON>(m_currentKey, rili::JSON()));
                m_value.object().back().second.number(n);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Root: {
            m_value.number(n);
            m_type = Type::None;
            return;
        }

        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleString(std::string&& s, size_t position) {
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::value || m_expectedToken == ExpectedTokenType::initial) {
                m_value.array().push_back(rili::JSON());
                m_value.array().back().string(s);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::keyOrEnd || m_expectedToken == ExpectedTokenType::key) {
                m_currentKey.swap(s);
                m_expectedToken = ExpectedTokenType::keySeparator;
                return;
            } else if (m_expectedToken == ExpectedTokenType::value) {
                m_value.object().emplace_back(std::pair<std::string, rili::JSON>(m_currentKey, rili::JSON()));
                m_value.object().back().second.string(s);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Root: {
            m_value.string(s);
            m_type = Type::None;
            return;
        }
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleObjectStart(size_t position, size_t maxDepth) {
    if (m_stack.size() >= maxDepth) {
        throw rili::JSON::SyntaxError(position);
    }
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::value || m_expectedToken == ExpectedTokenType::initial) {
                m_value.array().push_back(rili::JSON());
                m_value.array().back().object({});
                m_stack.emplace(m_value.array().back(), m_stack, Type::Object);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::value) {
                m_value.object().emplace_back(std::pair<std::string, rili::JSON>(m_currentKey, rili::JSON()));
                m_value.object().back().second.object({});
                m_stack.emplace(m_value.object().back().second, m_stack, Type::Object);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Root: {
            m_value.object({});
            m_stack.emplace(m_value, m_stack, Type::Object);
            m_type = Type::None;
            return;
        }
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleObjectEnd(size_t position) {
    switch (m_type) {
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::keyOrEnd ||
                m_expectedToken == ExpectedTokenType::separatorOrEnd) {
                m_stack.pop();
                return;
            }
            break;
        }
        case Type::Root:
        case Type::Array:
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleArrayStart(size_t position, size_t maxDepth) {
    if (m_stack.size() >= maxDepth) {
        throw rili::JSON::SyntaxError(position);
    }
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::value || m_expectedToken == ExpectedTokenType::initial) {
                m_value.array().push_back(rili::JSON());
                m_value.array().back().array({});
                m_stack.emplace(m_value.array().back(), m_stack, Type::Array);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::value) {
                m_value.object().emplace_back(std::pair<std::string, rili::JSON>(m_currentKey, rili::JSON()));
                m_value.object().back().second.array({});

                m_stack.emplace(m_value.object().back().second, m_stack, Type::Array);
                m_expectedToken = ExpectedTokenType::separatorOrEnd;
                return;
            }
            break;
        }
        case Type::Root: {
            m_value.array({});
            m_stack.emplace(m_value, m_stack, Type::Array);
            m_type = Type::None;
            return;
        }
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleArrayEnd(size_t position) {
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::separatorOrEnd || m_expectedToken == ExpectedTokenType::initial) {
                m_stack.pop();
                return;
            }
            break;
        }
        case Type::Object:
        case Type::Root:
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleElementSeparator(size_t position) {
    switch (m_type) {
        case Type::Array: {
            if (m_expectedToken == ExpectedTokenType::separatorOrEnd) {
                m_expectedToken = ExpectedTokenType::value;
                return;
            }
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::separatorOrEnd) {
                m_expectedToken = ExpectedTokenType::key;
                return;
            }
            break;
        }
        case Type::Root:
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

void Handler::handleObjectSeparator(size_t position) {
    switch (m_type) {
        case Type::Array: {
            break;
        }
        case Type::Object: {
            if (m_expectedToken == ExpectedTokenType::keySeparator) {
                m_expectedToken = ExpectedTokenType::value;
                return;
            }
            break;
        }
        case Type::Root:
        case Type::None: {
            break;
        }
    }
    throw rili::JSON::SyntaxError(position);
}

inline static char const* extractNumber(char const* start, char const* max, size_t position) {
    enum class State {
        Init,
        E,
        InitAfterMinus,
        SignAfterE,
        DigitAfterDot,
        LeadingZero,
        LeadingDigit,
        Dot,
        LeadingNonZeroDigit,
        DigitAfterE,
    };
    bool nonParsable = false;

    State state = State::Init;
    for (; nonParsable == false && start < max; start++) {
        switch (*start) {
            case '-': {
                if (state == State::Init) {
                    state = State::InitAfterMinus;
                } else if (state == State::E) {
                    state = State::SignAfterE;
                } else {
                    throw rili::JSON::SyntaxError(position);
                }
                break;
            }
            case '+': {
                if (state == State::E) {
                    state = State::SignAfterE;
                } else {
                    throw rili::JSON::SyntaxError(position);
                }
                break;
            }
            case '.': {
                if (state == State::LeadingZero || state == State::LeadingDigit ||
                    state == State::LeadingNonZeroDigit) {
                    state = State::Dot;
                } else {
                    throw rili::JSON::SyntaxError(position);
                }
                break;
            }
            case '0': {
                if (state == State::InitAfterMinus || state == State::Init) {
                    state = State::LeadingZero;
                } else if (state == State::LeadingNonZeroDigit) {
                    state = State::LeadingDigit;
                } else if (state == State::Dot) {
                    state = State::DigitAfterDot;
                } else if (state == State::E || state == State::SignAfterE) {
                    state = State::DigitAfterE;
                } else if (state == State::DigitAfterE || state == State::DigitAfterDot ||
                           state == State::LeadingDigit) {
                } else {
                    throw rili::JSON::SyntaxError(position);
                }
                break;
            }
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9': {
                if (state == State::InitAfterMinus || state == State::Init) {
                    state = State::LeadingNonZeroDigit;
                } else if (state == State::Dot) {
                    state = State::DigitAfterDot;
                } else if (state == State::LeadingNonZeroDigit) {
                    state = State::LeadingDigit;
                } else if (state == State::DigitAfterDot || state == State::LeadingDigit ||
                           state == State::DigitAfterE) {
                } else if (state == State::E || state == State::SignAfterE) {
                    state = State::DigitAfterE;
                } else {
                    throw rili::JSON::SyntaxError(position);
                }
                break;
            }
            case 'e':
            case 'E': {
                if (state == State::DigitAfterDot || state == State::LeadingZero || state == State::LeadingDigit ||
                    state == State::LeadingNonZeroDigit) {
                    state = State::E;
                } else {
                    throw rili::JSON::SyntaxError(position);
                }
                break;
            }
            default: {
                nonParsable = true;
                start--;
                break;
            }
        }
    }
    if (state != State::DigitAfterE && state != State::DigitAfterDot && state != State::LeadingZero &&
        state != State::LeadingDigit && state != State::LeadingNonZeroDigit) {
        throw rili::JSON::SyntaxError(position);
    } else {
        return start;
    }
}

inline static void reader(Handler::Stack& stack, const std::string& src, size_t maxDepth) {
    bool anyHandlerExecuted = false;
    char const* const startPosition = src.c_str();
    char const* position = src.c_str();
    char const* max = position + src.size();
    {
        const size_t invalidPosition = validateUtf8(reinterpret_cast<const unsigned char*>(src.c_str()), src.size());
        if (invalidPosition != src.size()) {
            throw rili::JSON::SyntaxError(invalidPosition);
        }
    }

    while (position < max) {
        if (*position == 0x20 || *position == 0x09 || *position == 0x0A || *position == 0x0D) {
            ++position;
        } else if ((*position >= '0' && *position <= '9') || *position == '-') {
            const char* pos = extractNumber(position, max, static_cast<size_t>(position - startPosition));
            std::string n(position, static_cast<size_t>(pos - position));
            anyHandlerExecuted = true;
            stack.top().handleNumber(std::move(n), static_cast<size_t>(position - startPosition));
            position = pos;
        } else {
            switch (*position) {
                case ',': {
                    anyHandlerExecuted = true;
                    stack.top().handleElementSeparator(static_cast<size_t>(position - startPosition));
                    ++position;
                    break;
                }
                case '"': {
                    char const* stringStart = ++position;
                    bool duringEscape = false;
                    for (; position < max; ++position) {
                        if (!duringEscape) {
                            if (*position == '\\') {
                                duringEscape = true;
                            } else if (*position == '\"') {
                                break;
                            }
                        } else {
                            duringEscape = false;
                        }
                    }
                    if (position < max) {
                        anyHandlerExecuted = true;
                        stack.top().handleString(jsonToUtf8(stringStart, static_cast<size_t>(position - stringStart),
                                                            static_cast<size_t>(position - startPosition)),
                                                 static_cast<size_t>(position - startPosition));
                        ++position;
                        break;
                    } else {
                        throw rili::JSON::SyntaxError(static_cast<size_t>(position - startPosition));
                    }
                }
                case 'n': {
                    if (max - position >= 4 && *(position + 1) == 'u' && *(position + 2) == 'l' &&
                        *(position + 3) == 'l') {
                        position += 4;
                        anyHandlerExecuted = true;
                        stack.top().handleNull(static_cast<size_t>(position - startPosition));
                        break;
                    }
                    throw rili::JSON::SyntaxError(static_cast<size_t>(position - startPosition));
                }
                case 't': {
                    if (max - position >= 4 && *(position + 1) == 'r' && *(position + 2) == 'u' &&
                        *(position + 3) == 'e') {
                        position += 4;
                        anyHandlerExecuted = true;
                        stack.top().handleBool(true, static_cast<size_t>(position - startPosition));
                        break;
                    }
                    throw rili::JSON::SyntaxError(static_cast<size_t>(position - startPosition));
                }
                case 'f': {
                    if (max - position >= 5 && *(position + 1) == 'a' && *(position + 2) == 'l' &&
                        *(position + 3) == 's' && *(position + 4) == 'e') {
                        position += 5;
                        anyHandlerExecuted = true;
                        stack.top().handleBool(false, static_cast<size_t>(position - startPosition));
                        break;
                    }
                    throw rili::JSON::SyntaxError(static_cast<size_t>(position - startPosition));
                }
                case ':': {
                    anyHandlerExecuted = true;
                    stack.top().handleObjectSeparator(static_cast<size_t>(position - startPosition));
                    ++position;
                    break;
                }
                case '[': {
                    anyHandlerExecuted = true;
                    stack.top().handleArrayStart(static_cast<size_t>(position - startPosition), maxDepth);
                    ++position;
                    break;
                }
                case ']': {
                    anyHandlerExecuted = true;
                    stack.top().handleArrayEnd(static_cast<size_t>(position - startPosition));
                    ++position;
                    break;
                }
                case '{': {
                    anyHandlerExecuted = true;
                    stack.top().handleObjectStart(static_cast<size_t>(position - startPosition), maxDepth);
                    ++position;
                    break;
                }
                case '}': {
                    anyHandlerExecuted = true;
                    stack.top().handleObjectEnd(static_cast<size_t>(position - startPosition));
                    ++position;
                    break;
                }
                default: { throw rili::JSON::SyntaxError(static_cast<size_t>(position - startPosition)); }
            }
        }
    }

    if (!anyHandlerExecuted) {
        throw rili::JSON::SyntaxError(0);
    }
    if (stack.empty()) {
        throw rili::JSON::SyntaxError(src.size());
    }
    stack.pop();
    if (!stack.empty()) {
        throw rili::JSON::SyntaxError(src.size());
    }
}

inline static std::string& stringifyImpl(std::string& result, rili::JSON const& v) noexcept {
    switch (v.type()) {
        case rili::JSON::Type::Null: {
            result += "null";
            break;
        }
        case rili::JSON::Type::Boolean: {
            result += (v.boolean() ? "true" : "false");
            break;
        }
        case rili::JSON::Type::String: {
            result += '"';
            utf8ToJson(result, v.string());
            result += '"';
            break;
        }
        case rili::JSON::Type::Number: {
            result += v.number();
            break;
        }
        case rili::JSON::Type::Array: {
            auto const& a = v.array();
            result += "[";
            for (auto it = a.begin(); it != a.end(); it++) {
                stringifyImpl(result, *it);
                if (std::next(it) != a.end()) {
                    result += ",";
                }
            }
            result += "]";
            break;
        }
        case rili::JSON::Type::Object: {
            auto const& o = v.object();
            result += "{";
            for (auto it = o.begin(); it != o.end(); it++) {
                result += '"';
                utf8ToJson(result, it->first);
                result += "\":";
                stringifyImpl(result, it->second);
                if (std::next(it) != o.end()) {
                    result += ",";
                }
            }
            result += "}";
            break;
        }
    }
    return result;
}
}  // namespace

namespace rili {
JSON::SyntaxError::SyntaxError(size_t position) noexcept
    : std::exception(),
      m_position(position),
      m_what("rili::JSON::SyntaxError at " + std::to_string(m_position)) {}

const char* JSON::SyntaxError::what() const noexcept { return m_what.c_str(); }

size_t JSON::SyntaxError::position() const noexcept { return m_position; }

void JSON::parse(const std::string& json, size_t maxDepth) {
    Handler::Stack stack;
    stack.emplace(*this, stack, Handler::Type::Root);
    reader(stack, json, maxDepth);
}

std::string JSON::stringify() const noexcept {
    std::string result;
    return stringifyImpl(result, *this);
}
}  // namespace rili
