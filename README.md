# Rili JSON

This is module of [rili](https://gitlab.com/rilis/rili) which contain implementation of JSON serializer and deserializer(parser).

**Documentation and API reference** :  [rilis.io](https://rilis.io/projects/rili/library/json)

**[Issue tracker](https://gitlab.com/rilis/rilis/issues)**

**Project status**

[![build status](https://gitlab.com/rilis/rili/json/badges/master/build.svg)](https://gitlab.com/rilis/rili/json/commits/master)
[![coverage report](https://gitlab.com/rilis/rili/json/badges/master/coverage.svg)](https://gitlab.com/rilis/rili/json/commits/master)
