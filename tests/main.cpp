#include <rili/Test.hpp>

int main(int, char**) { return rili::test::runner::run() ? 0 : 1; }
