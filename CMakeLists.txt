cmake_minimum_required(VERSION 3.0.0)
project(rili-json)

cmake_policy(SET CMP0054 NEW)

unset(MSVC)
if (${CMAKE_CXX_COMPILER_ID} STREQUAL "AppleClang" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wmissing-declarations -fno-rtti -fexceptions  -fstack-protector -pedantic -Wall -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdeprecated -Wextra -Winit-self -Wnon-virtual-dtor -Wno-padded -Wno-shadow -Wno-unused-macros -Wold-style-cast -Wpedantic -Wpointer-arith -Wsign-promo -Wstrict-aliasing -Wuninitialized")
elseif (${CMAKE_CXX_COMPILER_ID} STREQUAL "MSVC")
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /bigobj")
else()
  message(FATAL_ERROR "Unsupported compiler:${CMAKE_CXX_COMPILER_ID}")
endif()

option(RILI_JSON_AFL_FUZZER "If enabled, then will be created fuzzer executable" OFF)
option(RILI_JSON_COVERAGE "If enabled rili json will be compiled with coverage options" OFF)
option(RILI_JSON_TESTS "If enabled rili json tests will be build" OFF)

if(RILI_JSON_COVERAGE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
endif()

set(SOURCES
    rili/JSON.hpp
    rili/JSON.cpp
)

set(TESTS
  tests/main.cpp
  tests/Fixture.cpp
  tests/Fixture.hpp
  tests/JsonCheckerTests.cpp
  tests/NstJSONTestSuite.cpp
  tests/Examples.cpp
)

find_package(rili-compatibility REQUIRED)
find_package(rili-pattern REQUIRED)

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${rili-compatibility_INCLUDE_DIRS} ${rili-pattern_INCLUDE_DIRS})

add_library(${PROJECT_NAME} STATIC ${SOURCES})
target_link_libraries(${PROJECT_NAME} rili-compatibility rili-pattern)

install(TARGETS ${PROJECT_NAME}
  EXPORT ${PROJECT_NAME}Targets
  ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" COMPONENT staticlib)

install(DIRECTORY rili DESTINATION "${CMAKE_INSTALL_PREFIX}/include" FILES_MATCHING PATTERN "*.hpp")

set(INSTALL_CMAKE_CONFIG_DIR "${CMAKE_INSTALL_PREFIX}/lib/cmake/${PROJECT_NAME}")
configure_file("${CMAKE_CURRENT_SOURCE_DIR}/config.cmake.in" "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake" @ONLY)
install(EXPORT ${PROJECT_NAME}Targets DESTINATION "${INSTALL_CMAKE_CONFIG_DIR}" COMPONENT dev)
install(FILES "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake" DESTINATION "${INSTALL_CMAKE_CONFIG_DIR}" COMPONENT dev)


if(RILI_JSON_TESTS)
  add_definitions(-DRILI_JSON_SOURCE_TREE_PATH="${CMAKE_CURRENT_SOURCE_DIR}")
  find_package(rili-test REQUIRED)
  include_directories(tests ${rili-test_INCLUDE_DIRS})

  add_executable(${PROJECT_NAME}_tests ${TESTS})
  target_link_libraries(${PROJECT_NAME}_tests rili-test ${PROJECT_NAME})
endif()

set(CONFIG_FILES
  .clang-format
  .gitlab-ci.yml
  .hell.json
  .gitignore
  Doxyfile
  LICENSE.md
  README.md
)

if(RILI_JSON_AFL_FUZZER)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")
    add_executable(${PROJECT_NAME}-afl-fuzzer rili/JSON.cpp fuzzer/AFL.cpp)
endif()

add_custom_target(${PROJECT_NAME}-IDE SOURCES ${CONFIG_FILES} ${SOURCES})
